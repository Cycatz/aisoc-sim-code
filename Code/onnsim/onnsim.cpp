#include <systemc.h>

#include <cstdio>
#include <cstdlib>
#include <sstream>

class onnsim : public sc_module
{

public: 

    SC_HAS_PROCESS(onnsim);

    onnsim(sc_module_name name) : sc_module(name) {}
    void run(int argc, char *argv[])
    {
        FILE *fp;
        const int max_buf_len = 1024;
        char buf[max_buf_len + 1];
        char *val;
        std::string command;
        std::string output;
        std::string model_dir;

        if ((val = std::getenv("MODEL_DIR")) == nullptr) {
            fprintf(stderr, "ONNSim: can\'t found the environment variable \'MODEL_DIR\'\n");  
            exit(1);
        }
        model_dir = std::string(val);

        command = get_cmd(argc, argv, model_dir); 
        std::cout << command << endl;

        if ((fp = popen(command.c_str(), "r")) == nullptr) {
            perror("ONNSim: popen");  
            exit(1);
        }
        while (fgets(buf, max_buf_len, fp) != nullptr) {
            output += buf; 
        }

        std::cout << output << std::endl;
    }

private:

    std::string get_cmd(int argc, char *argv[], std::string &working_dir)
    {
        static const std::string docker_cmd = "/usr/bin/docker";
        static const std::string image_name = "onnc/cmsis:0.5";   
        static const std::string presim_cmd = "/umbrella/skymizer-master-toplevel/bin/precision_simulator.cortex_m";
        static const std::string testbench_outer_dir = "/home/cycatz/testbench";
        static const std::string testbench_inner_dir = "/umbrella/testbench";

        std::stringstream cmd_ss;

        cmd_ss << docker_cmd << " run -it --rm" \
               << " -v " << testbench_outer_dir << ":" << testbench_inner_dir \
               << " -w " << working_dir \
               << " " << image_name \
               << " " << presim_cmd;

        for (int i = 1; i < argc; i++) {
            cmd_ss << " " << argv[i];  
        }
        return cmd_ss.str();
    }
};

int sc_main(int argc, char *argv[])
{
    onnsim sim("onnsim1");
    sim.run(argc, argv);
    return 0;
}
