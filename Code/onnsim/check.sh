#!/bin/bash

TESTBENCH_OUTER_DIR="$HOME/testbench"
TESTBENCH_INNER_DIR="/umbrella/testbench"
ONNSIM_DIR="$HOME/onnc-umbrella/external/systemc/examples/sysc/onnsim"
ONNSIM_BIN="$ONNSIM_DIR/onnsim.x"

declare -A TESTBENCH_CMDS

TESTBENCH_CMDS=(
    ["ad"]="$ONNSIM_BIN ad01.onnx -ctable-path=ctable.json -input-data=ad01_1x640.pb -o onnsim_output_ad.bin > /dev/null 2>&1"
    ["ic"]="$ONNSIM_BIN pretrainedResnet.onnx -ctable-path=ctable.json -input-data=ic_1x3x32x32.pb -o onnsim_output_ic.bin > /dev/null 2>&1"
    ["kws"]="$ONNSIM_BIN kws_ref_model.onnx -ctable-path=ctable.json -input-data=kws01_1x1x49x10.pb -o onnsim_output_kws.bin > /dev/null 2>&1"
    ["vww"]="$ONNSIM_BIN vww_96.onnx -ctable-path=ctable.json -input-data=vw_coco2014_batch1.pb -o onnsim_output_vww.bin > /dev/null 2>&1"
)

run_image() {
    /usr/bin/docker run -it --rm \
        -v "$TESTBENCH_OUTER_DIR":"$TESTBENCH_INNER_DIR" \
        onnc/cmsis:0.5 $@
}

generate_correct_outputs() {
    run_image "/usr/bin/make -C $TESTBENCH_INNER_DIR" > /dev/null 2>&1
}


check_onnsim_outputs() {
    make -C "$ONNSIM_DIR" > /dev/null 2>&1

    for model in "${!TESTBENCH_CMDS[@]}"; do
        local cmd

        cmd="${TESTBENCH_CMDS[$model]}"

        MODEL_OUTER_DIR="${TESTBENCH_OUTER_DIR}/${model}"
        MODEL_DIR="${TESTBENCH_INNER_DIR}/${model}" 

        export MODEL_DIR; eval "$cmd"

        diff "${MODEL_OUTER_DIR}/onnsim_output_${model}.bin" "${MODEL_OUTER_DIR}/output.bin"

        if [[ "$?" -eq 0 ]]; then
            printf "[\033[1;32mPassed\033[0m] " 
            printf "Model: %s\n" "${model}" 
        else
            printf "[\033[1;31mPassed\033[0m] " 
            printf "Model: %s\n" "${model}" 
        fi
    done
}

main() {
    generate_correct_outputs    
    check_onnsim_outputs
}

main "$@"
