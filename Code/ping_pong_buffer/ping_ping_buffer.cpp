/*****************************************************************************
  Licensed to Accellera Systems Initiative Inc. (Accellera) under one or
  more contributor license agreements.  See the NOTICE file distributed
  with this work for additional information regarding copyright ownership.
  Accellera licenses this file to you under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with the
  License.  You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
  implied.  See the License for the specific language governing
  permissions and limitations under the License.
 *****************************************************************************/

/*****************************************************************************
  simple_ping_pong.cpp -- Simple SystemC 2.0 producer/consumer  example.
                     From "An Introduction to System Level Modeling in
                     SystemC 2.0". By Stuart Swan, Cadence Design Systems.
                     Available at www.accellera.org
  Original Author: Stuart Swan, Cadence Design Systems, 2001-06-18
 *****************************************************************************/

/*****************************************************************************
  MODIFICATION LOG - modifiers, enter your name, affiliation, date and
  changes you are making here.
      Name, Affiliation, Date:
  Description of Modification:
 *****************************************************************************/


#include <systemc.h>

class write_if : virtual public sc_interface
{
public:
    virtual void write(char, bool) = 0;
    virtual void reset() = 0;
};

class read_if : virtual public sc_interface
{
public:
    virtual void read(char &) = 0;
    virtual int num_available(int) = 0;
};

class noti_if : virtual public sc_interface
{
public:
    virtual void notify() = 0;
};



// id: writer  
// id ^ 1: reader  
class ping_pong : public sc_channel, public write_if, public read_if, public noti_if
{
public:
    ping_pong(sc_module_name name) : sc_channel(name), id(0), writer_left(0), finish(false) {
        memset(num_elements, 0, sizeof(num_elements));
    }

    void write(char c, bool _finish)
    {
        if (num_available(id) == max)
            wait(full_event);
        data[id][num_available(id)] = c;
        ++num_elements[id];
        finish = _finish; 
        writer_left = num_available(id); 
    }

    void read(char &c)
    {
        if (finish && num_available(id ^ 1) == 0) {
            wait(left_event);
            c = data[id][writer_left - num_available(id)];
            --num_elements[id];
        } else {
            if (num_available(id ^ 1) == 0)
                wait(full_event);
            c = data[id ^ 1][max - num_available(id ^ 1)];
            --num_elements[id ^ 1];
        }
    }

    void notify()
    {
        if (num_available(id ^ 1) == 0 && num_available(id) == max) {
            id ^= 1;
            full_event.notify();
        }
        if (finish && num_available(id ^ 1) == 0 && num_available(id) > 0) {
            left_event.notify(); 
        }
    }

    void reset() {
        memset(num_elements, 0, sizeof(num_elements));
        id = writer_left = 0;
        finish = false; 
    }

    int num_available(int flag) { return num_elements[flag]; }

private:
    enum e { max = 10 };
    char data[2][max];
    int num_elements[2]; 
    int id;
    int writer_left;
    bool finish;
    sc_event full_event, left_event;  
};

class producer : public sc_module
{
public:
    sc_in_clk clk;  // Clock input of the design
    sc_port<write_if> out;

    SC_HAS_PROCESS(producer);

    producer(sc_module_name name) : sc_module(name)
    {
        SC_THREAD(main);
        sensitive << clk.pos();
    }

    void main()
    {
        const char *str =
            "Visit www.accellera.org and see what SystemC can do for you "
            "today!\n";

        while (*str) {
            wait();
            char ch = *str++;  
            out->write(ch, (*str == '\0'));
        }
    }
};

class consumer : public sc_module
{
public:
    sc_in_clk clk;  // Clock input of the design
    sc_port<read_if> in;

    SC_HAS_PROCESS(consumer);

    consumer(sc_module_name name) : sc_module(name)
    {
        SC_THREAD(main);
        sensitive << clk.pos();
    }

    void main()
    {
        char c;
        cout << endl << endl;

        while (true) {
            wait();
            in->read(c);
            cout << c << flush;

            // if (in->num_available() == 1)
            //     cout << "<1>" << flush;
            // if (in->num_available() == 9)
            //     cout << "<9>" << flush;
        }
    }
};

class notifier : public sc_module
{
public:
    sc_in_clk clk;  // Clock input of the design
    sc_port<noti_if> noti;

    SC_HAS_PROCESS(notifier);

    notifier(sc_module_name name) : sc_module(name)
    {
        SC_THREAD(main);
        sensitive << clk.pos();
    }

    void main()
    {
        while (true) {
            wait();
            noti->notify();
        }
    }
};

class top : public sc_module
{
public:
    ping_pong *ping_pong_inst;
    producer *prod_inst;
    consumer *cons_inst;
    notifier *noti_inst;
    sc_clock *clk;

    top(sc_module_name name) : sc_module(name)
    {
        clk = new sc_clock();
        ping_pong_inst = new ping_pong("Ping_Pong1");

        noti_inst = new notifier("Notifier");
        noti_inst->clk(*clk);
        noti_inst->noti(*ping_pong_inst);

        prod_inst = new producer("Producer1");
        prod_inst->clk(*clk);
        prod_inst->out(*ping_pong_inst);

        cons_inst = new consumer("Consumer1");
        cons_inst->clk(*clk);
        cons_inst->in(*ping_pong_inst);

    }
};

int sc_main(int, char *[])
{
    top top1("Top1");
    sc_start(100, SC_NS);
    return 0;
}
